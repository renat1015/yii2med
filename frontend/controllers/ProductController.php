<?php

namespace frontend\controllers;

use yii\web\Controller;
use app\models\Product;
use app\models\Images;
use app\models\Categories;
//use yii\data\Pagination;

class ProductController extends \yii\web\Controller
{
    public function actionIndex($id)
    {
        if ($id == NULL) {
            $data = Product::getProductByCategory(NULL);
        } else $data = Product::getProductByCategory($id);
	    $categories = Categories::getAll();

	    return $this->render('index', [
	         'products' => $data['products'],
	         'pages' => $data['pages'],
	         'categories' => $categories,
	    ]);
    }

    public function actionView($id)
    {
        $product = Product::findOne($id);

        return $this->render('view', ['product'=>$product]);
    }

}