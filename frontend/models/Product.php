<?php

namespace app\models;

use Yii;
use yii\data\Pagination;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string|null $name
 * @property int|null $category_id
 * @property string|null $price
 * @property string|null $description
 *
 * @property Categories $category
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id'], 'integer'],
            [['description'], 'string'],
            [['name', 'price'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Categories::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'category_id' => 'Category ID',
            'price' => 'Price',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(Categories::className(), ['id' => 'category_id']);
    }

    public static function getProductByCategory($id)
    {
        if ($id == null) {
            $query = Product::find();
        } else $query = Product::find()->where(['category_id' => $id]);

        $countQuery = clone $query;
        $pages = new Pagination(['totalCount' => $countQuery->count(), 'pageSize'=>6]);
        $products = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();

        $data['products'] = $products;
        $data['pages'] = $pages;

        return $data;
    }
}
