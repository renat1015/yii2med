<?php

use yii\helpers\Url;

?>

<div class="col-md-3">
    <h2>Категории</h2>
    <ul>
        <li>
            <a href="<?= Url::toRoute(['product/index', 'id'=> '']);?>">Все категории</a>
        </li>
        <?php foreach ($categories as $category): ?> 
        <li>
            <a href="<?= Url::toRoute(['product/index', 'id'=>$category->id]);?>"><?= $category->category?></a>
        </li>
        <?php endforeach; ?>
    </ul>    
</div>