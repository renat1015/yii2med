<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\Images;
/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товар';
$this->params['breadcrumbs'][] = $this->title;

?>

<head>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <!-- fotorama.css & fotorama.js. -->
    <link href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet">
    <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script>
</head>

<!--main content start-->
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="row">
                    <div class="fotorama">
                        <?= Html::img('@web/uploads/' . Images::getFirstImageByProduct($product->id, 0), ['alt' => 'Изображение товара']) ?>
                        <?= Html::img('@web/uploads/' . Images::getFirstImageByProduct($product->id, 1), ['alt' => 'Изображение товара']) ?>
                        <?= Html::img('@web/uploads/' . Images::getFirstImageByProduct($product->id, 2), ['alt' => 'Изображение товара']) ?>
                        <?= Html::img('@web/uploads/' . Images::getFirstImageByProduct($product->id, 3), ['alt' => 'Изображение товара']) ?>
                        <?= Html::img('@web/uploads/' . Images::getFirstImageByProduct($product->id, 4), ['alt' => 'Изображение товара']) ?>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div>
                    <h2>Наименование: <strong><?= $product->name?></strong></h2>
                </div>
                <div>
                    <h3>Цена: <?= $product->price?> руб.</h3>
                </div>
                <br>
                <div>
                    <h4>Описание:</h4>
                    <p><?= $product->description?></p>
                </div>
            </div>  
        </div>
    </div>
</div>
<!-- end main content-->