<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\LinkPager;
use app\models\Product;
use app\models\Images;
use app\models\Categories;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Товары';
$this->params['breadcrumbs'][] = $this->title;
?>

<!--main content start-->
<div class="main">
    <div class="container">
        <div class="row">
            <?= $this->render('/partials/sidebar', [
                'categories' => $categories,
            ]); ?>
            <div class="col-md-9">
                <?php foreach ($products as $product): ?>
                    <div class="col-md-6">
                        <h3><a href="<?= Url::toRoute(['product/view', 'id'=>$product->id]);?>"><?= $product->name?></a></h3>
                        <h4>Цена: <?= $product->price?> руб.</h4>
                        <a href="<?= Url::toRoute(['product/view', 'id'=>$product->id]);?>"><?= Html::img('@web/uploads/' . Images::getFirstImageByProduct($product->id, 0), ['alt' => 'Изображение товара', 'width' => '350']);?></a>
                        <br/>
                        <br/>
                    </div>
                <?php endforeach; ?>
            </div> 
            <?php
                echo LinkPager::widget([
                    'pagination' => $pages,
                ]);
            ?> 
        </div>
    </div>
</div>
<!-- end main content-->