<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
    /**
     * @var UploadedFile[]
     */
    public $imageFiles;

    public function rules()
    {
        return [
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 20],
        ];
    }
    
    public function upload()
    {
        if ($this->validate())
        { 
            $arr = [];
            foreach ($this->imageFiles as $file) {
                $x = md5(uniqid($file->baseName)). '.' . $file->extension;
                $file->saveAs(Yii::getAlias('@img') . '/uploads/' . $x);
                $arr[] = $x;
            }
            return $arr;
            return true;
        } else {
            return false;
        }
    }
}






























/*namespace app\models;

use Yii;
use yii\base\Model;
use yii\web\UploadedFile;

class ImageUpload extends Model
{
    
    public $imageFile;

    public function uploadFile()
    {
        //$file->saveAs(Yii::getAlias('@web' . 'uploads/' . $file->name));
        //var_dump($file);die;


        if ($this->validate()) {
            $this->image->saveAs('@backend/web/uploads/' . $this->image->baseName . '.' . $this->image->extension);
            return true;
        } else {
            return false;
        }
        var_dump($file);die;
    }

}
*/