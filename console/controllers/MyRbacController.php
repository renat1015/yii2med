<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
/**
 * Инициализатор RBAC выполняется в консоли php yii my-rbac/init
 */
class MyRbacController extends Controller {

    public function actionInit() {
        $auth = Yii::$app->authManager;
        
        $auth->removeAll(); //На всякий случай удаляем старые данные из БД...
        
        // Создадим роли админа, менеджера и покупателя
        $admin = $auth->createRole('admin');
        $manager = $auth->createRole('manager');
        $buyer = $auth->createRole('buyer');
        
        // запишем их в БД
        $auth->add($admin);
        $auth->add($manager);
        $auth->add($buyer);
        
        // Создаем разрешения
        $userAccess = $auth->createPermission('/admin/*');
        $userAccess->description = 'Доступ к rbac';
        
        $categoryAccess = $auth->createPermission('/categories/*');
        $categoryAccess->description = 'Редактирование категорий';

        $productAccess = $auth->createPermission('/products/*');
        $productAccess->description = 'Редактирование товара';

        $buyAccess = $auth->createPermission('/product/*');
        $buyAccess->description = 'Просмотр товара';
        
        // Запишем эти разрешения в БД
        $auth->add($userAccess);
        $auth->add($categoryAccess);
        $auth->add($productAccess);
        $auth->add($buyAccess);
        
        // Теперь добавим наследования
        $auth->addChild($buyer,$buyAccess);

        $auth->addChild($manager, $buyer);
        $auth->addChild($manager, $categoryAccess);
        $auth->addChild($manager, $productAccess);

        $auth->addChild($admin, $manager);
        $auth->addChild($admin, $userAccess);

        // Назначаем роль admin пользователю с ID 1
        $auth->assign($admin, 1); 
        
        // Назначаем роль manager пользователю с ID 2
        $auth->assign($manager, 2);

        // Назначаем роль buyer пользователю с ID 3
        $auth->assign($buyer, 3);
    }
}