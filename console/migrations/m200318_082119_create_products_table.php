<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 */
class m200318_082119_create_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%products}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'category_id' => $this->integer(),
            'price' => $this->string(),
            'description' => $this->text()
        ]);

        $this->createIndex(
            'idx-products-category_id' ,
            'products' ,
            'category_id'
        );

        $this->addForeignKey(
            'fk-products-category_id' ,
            'products' ,
            'category_id' ,
            'categories' ,
            'id' ,
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%products}}');
    }
}
