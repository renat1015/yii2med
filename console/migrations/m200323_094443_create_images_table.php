<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%images}}`.
 */
class m200323_094443_create_images_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%images}}', [
            'id' => $this->primaryKey(),
            'product_id' => $this->integer(),
            'image_name' => $this->string()
        ]);

        $this->createIndex(
            'idx-images-product_id' ,
            'images' ,
            'product_id'
        );

        $this->addForeignKey(
            'fk-images-product_id' ,
            'images' ,
            'product_id' ,
            'products' ,
            'id' ,
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%images}}');
    }
}
