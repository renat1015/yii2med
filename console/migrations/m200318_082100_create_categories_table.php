<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%categories}}`.
 */
class m200318_082100_create_categories_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%categories}}', [
            'id' => $this->primaryKey(),
            'category' => $this->string()
        ]);

        $this->insert( 'categories' , [
            'category' => 'Категория 1'
        ]);

        $this->insert( 'categories' , [
            'category' => 'Категория 2'
        ]);

        $this->insert( 'categories' , [
            'category' => 'Категория 3'
        ]);

        $this->insert( 'categories' , [
            'category' => 'Категория 4'
        ]);

        $this->insert( 'categories' , [
            'category' => 'Категория 5'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%categories}}');
    }
}
